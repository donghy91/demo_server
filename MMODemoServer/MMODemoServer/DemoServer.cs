﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Diagnostics;

using MySql.Data.MySqlClient;

namespace MMODemoServer
{
    class DemoServer
    {
        readonly int MAX_BACKLOG_NUM = 10;
        readonly int MMODEMOSERVER_PORT = 8253;

        static X509Certificate serverCertificate = null;

        public class AsyncObject
        {
            public Byte[] Buffer;
            public Socket WorkingSocket;
            public AsyncObject() { }
            public AsyncObject(Int32 bufferSize)
            {
                this.Buffer = new Byte[bufferSize];
            }
        }

        private Socket m_ServerSocket = null;
        private List<Socket> m_ClientSocket;

        private AsyncCallback m_fnAcceptHandler;
        private AsyncCallback m_fnSendHandler;
        private AsyncCallback m_fnReceiveHandler;
        private MySqlConnection m_dbConn;

        public DemoServer()
        {
            m_ClientSocket = new List<Socket>();
            m_fnAcceptHandler = new AsyncCallback(HandleClientConnectionRequest);
            m_fnSendHandler = new AsyncCallback(HandleDataSend);
            m_fnReceiveHandler = new AsyncCallback(HandleDataReceive);
            m_dbConn = new MySqlConnection("Server=143.248.233.53;Database=mmo_login;Uid=root;Pwd=mmo;");
        }

        public void StartServer()
        {
            try
            {
                serverCertificate = new X509Certificate(@"CARoot.pfx", "82538253");
                TcpListener listener = new TcpListener(IPAddress.Any, 8253);
                listener.Start();

                while(true)
                {
                    Console.WriteLine("Waiting for a client to connect...");
                    // Application blocks while waiting for an incoming connection.
                    // Type CNTL-C to terminate the server.
                    TcpClient client = listener.AcceptTcpClient();
                    ProcessClient(client);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("errer startServer : {0}", ex.Message);
            }

            /*
            m_ServerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            m_ServerSocket.Bind(new IPEndPoint(IPAddress.Any, MMODEMOSERVER_PORT));
            m_ServerSocket.Listen(MAX_BACKLOG_NUM);
            m_ServerSocket.BeginAccept(m_fnAcceptHandler, null);
            try
            {
                m_dbConn.Open();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
            */
        }

        static void ProcessClient(TcpClient client)
        {
            // A client has connected. Create the 
            // SslStream using the client's network stream.
            SslStream sslStream = new SslStream(
                client.GetStream(), false);
            // Authenticate the server but don't require the client to authenticate.
            try
            {
                sslStream.AuthenticateAsServer(serverCertificate,
                    false, SslProtocols.Tls, true);

                Console.WriteLine("Encryption: {0}", sslStream.IsEncrypted);
                Console.WriteLine("Cipher: {0} strength {1}", sslStream.CipherAlgorithm, sslStream.CipherStrength);
                Console.WriteLine("Hash: {0} strength {1}", sslStream.HashAlgorithm, sslStream.HashStrength);
                Console.WriteLine("Key exchange: {0} strength {1}", sslStream.KeyExchangeAlgorithm, sslStream.KeyExchangeStrength);
                Console.WriteLine("Protocol: {0}", sslStream.SslProtocol);

                // Set timeouts for the read and write to 5 seconds.
                sslStream.ReadTimeout = 5000;
                sslStream.WriteTimeout = 5000;
                // Read a message from the client.   
                Console.WriteLine("Waiting for client message...");
                string messageData = ReadMessage(sslStream);
                Console.WriteLine("Received: {0}", messageData);

                // Write a message to the client.
                byte[] message = Encoding.UTF8.GetBytes("1<EOF>");
                Console.WriteLine("Sending hello message.");
                sslStream.Write(message);
            }
            catch (AuthenticationException e)
            {
                Console.WriteLine("Exception: {0}", e.Message);
                if (e.InnerException != null)
                {
                    Console.WriteLine("Inner exception: {0}", e.InnerException.Message);
                }
                Console.WriteLine("Authentication failed - closing the connection.");
                sslStream.Close();
                client.Close();
                return;
            }
            finally
            {
                // The client stream will be closed with the sslStream
                // because we specified this behavior when creating
                // the sslStream.
                sslStream.Close();
                client.Close();
            }
        }

        static string ReadMessage(SslStream sslStream)
        {
            // Read the  message sent by the client.
            // The client signals the end of the message using the
            // "<EOF>" marker.
            byte[] buffer = new byte[2048];
            StringBuilder messageData = new StringBuilder();
            int bytes = -1;
            do
            {
                // Read the client's test message.
                bytes = sslStream.Read(buffer, 0, buffer.Length);

                // Use Decoder class to convert from bytes to UTF8
                // in case a character spans two buffers.
                Decoder decoder = Encoding.UTF8.GetDecoder();
                char[] chars = new char[decoder.GetCharCount(buffer, 0, bytes)];
                decoder.GetChars(buffer, 0, bytes, chars, 0);
                messageData.Append(chars);
                // Check for EOF or an empty message.
                if (messageData.ToString().IndexOf("<EOF>") != -1)
                {
                    break;
                }
            } while (bytes != 0);

            return messageData.ToString();
        }


        public void SendMessage(string message, Socket target)
        {
            AsyncObject ao = new AsyncObject();

            ao.Buffer = Encoding.Unicode.GetBytes(message);
            ao.WorkingSocket = target;

            try
            {
                target.BeginSend(ao.Buffer, 0, ao.Buffer.Length, SocketFlags.None, m_fnSendHandler, ao);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception SendMessage : {0}", ex.Message);
            }
        }

        public void StopServer()
        {
            try
            {
                m_dbConn.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }

            m_ServerSocket.Close();
        }

        private void HandleClientConnectionRequest(IAsyncResult ar)
        {
            Socket clientSocket;
            try
            {
                clientSocket = m_ServerSocket.EndAccept(ar);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception HandleClientConnectionRequest : {0}", ex.Message);
                return;
            }

            AsyncObject ao = new AsyncObject(4096);
            ao.WorkingSocket = clientSocket;
            m_ClientSocket.Add(clientSocket);

            try
            {
                clientSocket.BeginReceive(ao.Buffer, 0, ao.Buffer.Length, SocketFlags.None, m_fnReceiveHandler, ao);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception HandleClientConnectionRequest : {0}", ex.Message);
                return;
            }

        }

        private void HandleDataReceive(IAsyncResult ar)
        {
            AsyncObject ao = (AsyncObject)ar.AsyncState;

            Int32 recvByte;
            try
            {
                recvByte = ao.WorkingSocket.EndReceive(ar);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception HandleDataReceive : {0}", ex.Message);
                return;
            }

            if (recvByte > 0)
            {
                Byte[] msgByte = new Byte[recvByte];
                Array.Copy(ao.Buffer, msgByte, recvByte);

                string msg = Encoding.Unicode.GetString(msgByte);

                Console.WriteLine("MessageRecv : {0}", msg);
                string[] info = msg.Split(',');

                foreach (string s in info)
                {
                    Console.WriteLine(s);
                }

                MySqlCommand userSearcher = new MySqlCommand();
                userSearcher.Connection = m_dbConn;
                userSearcher.CommandText = "SELECT id FROM users where id=@id and password=@password";
                userSearcher.Parameters.Add("@id", MySqlDbType.VarChar, 10);
                userSearcher.Parameters.Add("@password", MySqlDbType.VarChar, 10);
                userSearcher.Parameters[0].Value = info[0];
                userSearcher.Parameters[1].Value = info[1];
                
                if (!userSearcher.ExecuteReader().Read())
                {
                    Console.WriteLine("No Such User Exist!");
                    return;
                }               

                SendMessage("Hello " + info[0], ao.WorkingSocket);

            }

            try
            {
                ao.WorkingSocket.BeginReceive(ao.Buffer, 0, ao.Buffer.Length, SocketFlags.None, m_fnReceiveHandler, ao);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception HandleClientConnectionRequest : {0}", ex.Message);
                return;
            }

        }

        private void HandleDataSend(IAsyncResult ar)
        {
            AsyncObject ao = (AsyncObject)ar.AsyncState;
            Int32 sentBytes;

            try
            {
                sentBytes = ao.WorkingSocket.EndSend(ar);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception HandleDataSend : {0}", ex.Message);
                return;
            }

            if(sentBytes > 0)
            {
                Byte[] msgByte = new Byte[sentBytes];
                Array.Copy(ao.Buffer, msgByte, sentBytes);

                Console.WriteLine("MessageSend : {0}", Encoding.Unicode.GetString(msgByte));
            }
        }

    }
}
