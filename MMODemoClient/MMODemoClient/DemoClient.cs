﻿using System;
using System.Collections;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.IO;

namespace MMODemoClient
{
    class DemoClient
    {
        readonly string MMODEMOSERVER_IP = "127.0.0.1";
        readonly int MMODEMOSERVER_PORT = 8253;

        private Hashtable certificateErrors = new Hashtable();

        public bool ValidateServerCertificate(
              object sender,
              X509Certificate certificate,
              X509Chain chain,
              SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == SslPolicyErrors.None)
                return true;

            Console.WriteLine("Certificate error: {0}", sslPolicyErrors);

            // Do not allow this client to communicate with unauthenticated servers.
            return false;

        }


        public class AsyncObject
        {
            public Byte[] Buffer;
            public Socket WorkingSocket;

            public AsyncObject() { }
            public AsyncObject(Int32 bufferSize)
            {
                this.Buffer = new Byte[bufferSize];
            }
        }

        private bool m_bIsConnected;
        private Socket m_ClientSocket = null;
        private AsyncCallback m_fnReceiveHandler = null;
        private AsyncCallback m_fnSendHandler = null;

        public DemoClient ()
        {
            m_fnReceiveHandler = new AsyncCallback(HandleDataReceive);
            m_fnSendHandler = new AsyncCallback(HandleDataSend);
        }

        public void ConnectToServer()
        {
            // Create a TCP/IP client socket.
            // machineName is the host running the server application.
            TcpClient client = new TcpClient(MMODEMOSERVER_IP, MMODEMOSERVER_PORT);
            Console.WriteLine("Client connected.");
            // Create an SSL stream that will close the client's stream.
            SslStream sslStream = new SslStream(
                client.GetStream(),
                false,
                new RemoteCertificateValidationCallback(ValidateServerCertificate),
                null
                );
            // The server name must match the name on the server certificate.
            try
            {
                sslStream.AuthenticateAsClient("CARoot");
            }
            catch (AuthenticationException e)
            {
                Console.WriteLine("Exception: {0}", e.Message);
                if (e.InnerException != null)
                {
                    Console.WriteLine("Inner exception: {0}", e.InnerException.Message);
                }
                Console.WriteLine("Authentication failed - closing the connection.");
                client.Close();
                return;
            }
            // Encode a test message into a byte array.
            // Signal the end of the message using the "<EOF>".
            byte[] messsage = Encoding.UTF8.GetBytes("1 <EOF>");
            // Send hello message to the server. 
            sslStream.Write(messsage);
            sslStream.Flush();
            // Read message from the server.
            string serverMessage = ReadMessage(sslStream);
            Console.WriteLine("Server says: {0}", serverMessage);
            // Close the client connection.
            client.Close();
            Console.WriteLine("Client closed.");
            /*
            m_ClientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            m_bIsConnected = false;
            try
            {
                m_ClientSocket.Connect(MMODEMOSERVER_IP, MMODEMOSERVER_PORT);
                m_bIsConnected = true;
            }
            catch(Exception ex)
            {
                Console.WriteLine("ConnetToServer Err : {0}",ex.Message);
            }
            
            if(m_bIsConnected)
            {
                AsyncObject ao = new AsyncObject(4096);
                ao.WorkingSocket = m_ClientSocket;
                m_ClientSocket.BeginReceive(ao.Buffer, 0, ao.Buffer.Length, SocketFlags.None, m_fnReceiveHandler, ao);

                Console.WriteLine("ConnetToServer Success");
            }
            else
            {
                Console.WriteLine("ConnetToServer Fail");
            }
            */
        }

        static string ReadMessage(SslStream sslStream)
        {
            // Read the  message sent by the server.
            // The end of the message is signaled using the
            // "<EOF>" marker.
            byte[] buffer = new byte[2048];
            StringBuilder messageData = new StringBuilder();
            int bytes = -1;
            do
            {
                bytes = sslStream.Read(buffer, 0, buffer.Length);

                // Use Decoder class to convert from bytes to UTF8
                // in case a character spans two buffers.
                Decoder decoder = Encoding.UTF8.GetDecoder();
                char[] chars = new char[decoder.GetCharCount(buffer, 0, bytes)];
                decoder.GetChars(buffer, 0, bytes, chars, 0);
                messageData.Append(chars);
                // Check for EOF.
                if (messageData.ToString().IndexOf("<EOF>") != -1)
                {
                    break;
                }
            } while (bytes != 0);

            return messageData.ToString();
        }

        public void StopClient()
        {
            m_ClientSocket.Close();
        }

        public void SendLoginPacket(string id, string password)
        {
            AsyncObject ao = new AsyncObject();
            ao.Buffer = Encoding.Unicode.GetBytes(id + "," + password);
            ao.WorkingSocket = m_ClientSocket;

            try
            {
                m_ClientSocket.BeginSend(ao.Buffer, 0, ao.Buffer.Length, SocketFlags.None, m_fnSendHandler, ao);
            }
            catch (Exception ex)
            {
                Console.WriteLine("SendLoginPacket Err : {0}", ex.Message);
            }
        }



        private void HandleDataReceive(IAsyncResult ar)
        {
            AsyncObject ao = (AsyncObject)ar.AsyncState;

            Int32 recvBytes;

            try
            {
                recvBytes = ao.WorkingSocket.EndReceive(ar);
            }
            catch (Exception ex)
            {
                Console.WriteLine("HandleDataSend err: {0}", ex.Message);
                return;
            }

            if (recvBytes > 0)
            {
                Byte[] msgByte = new Byte[recvBytes];
                Array.Copy(ao.Buffer, msgByte, recvBytes);

                Console.WriteLine("Server said... : {0}", Encoding.Unicode.GetString(msgByte));
            }
        }

        private void HandleDataSend(IAsyncResult ar)
        {
            AsyncObject ao = (AsyncObject)ar.AsyncState;
            
            Int32 sentBytes;

            try
            {
                sentBytes = ao.WorkingSocket.EndSend(ar);
            }
            catch (Exception ex)
            {
                Console.WriteLine("HandleDataSend err: {0}", ex.Message);
                return;
            }

            if (sentBytes > 0)
            {
                Byte[] msgByte = new Byte[sentBytes];
                Array.Copy(ao.Buffer, msgByte, sentBytes);

                Console.WriteLine("Message Send : {0}", Encoding.Unicode.GetString(msgByte));
            }
        }

    }
}
